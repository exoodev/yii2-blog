<?php

use yii\helpers\Html;

$this->title = Html::encode(Yii::$app->settings->get('blog', 'titleBlog'));
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['post/index']];
$this->params['heading'] = $this->title;

echo $this->render('/items/list', [
    'dataProvider' => $dataProvider,
]);
