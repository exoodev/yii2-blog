<?php

use exoo\blog\widgets\CategoryNav;
use yii\helpers\Html;

$this->title = Html::encode($model->title);
$this->params['heading'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Blog'), 'url' => ['post/index']];

if (isset($model->category)) {
    $this->params['breadcrumbs'][] = ['label' => $model->category->title, 'url' => ['category/view', 'slug' => $model->category->slug]];
}
$this->params['breadcrumbs'][] = $this->title;

$params = Yii::$app->settings;
$meta = [];

if ($params->get('blog', 'showAuthor')) {
    $meta[] = $model->author->username;
}

if ($params->get('blog', 'showPublishDate')) {
    $meta[] = Yii::$app->formatter->asDate($model->publish_on, 'long');
}
echo CategoryNav::widget();
?>

<p>
    <?php if ($image = $model->getImageUrl('large')): ?>
    <?= Html::img($image, ['alt' => $this->title]) ?>
    <?php endif; ?>
</p>

<h1 class="uk-article-title"><?= $this->title ?></h1>

<?php if ($meta): ?>
<p class="uk-article-meta"><?= implode(' | ', $meta) ?></p>
<?php endif; ?>

<p><?= $model->full_text ?></p>