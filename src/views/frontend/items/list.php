<?php

use exoo\uikit\ListView;
use exoo\blog\widgets\CategoryNav;

$optionsListView = [
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'summary' => false,
];
$columns = Yii::$app->settings->get('blog', 'postsColumns');

if ($columns > 1) {
    $optionsListView['options'] = [
        'class' => 'uk-child-width-1-' . $columns . '@m',
        'uk-grid' => true,
        'uk-height-match' => 'target: .uk-card'
    ];
} else {
    $optionsListView['itemOptions'] = [
        'class' => 'uk-grid-margin',
    ];
}

echo ListView::widget($optionsListView);
echo CategoryNav::widget();
