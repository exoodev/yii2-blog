<?php

$meta = [];
$link = ['view', 'slug' => $model->slug];

if ($model->category_id !== null) {
    $link['category'] = $model->category->slug;
}
if (Yii::$app->settings->get('blog', 'showAuthor')) {
    $meta[] = $model->author->username;
}
if (Yii::$app->settings->get('blog', 'showPublishDate')) {
    $meta[] = Yii::$app->formatter->asDate($model->publish_on, 'long');
}

echo $this->render('layouts/_' . Yii::$app->settings->get('blog', 'postsLayout', 'horizontal'), [
    'meta' => $meta,
    'link' => $link,
    'model' => $model,
]);
