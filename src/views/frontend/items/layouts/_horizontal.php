<?php
use yii\helpers\Html;

$title = Html::encode($model->title);
?>

<div class="uk-card uk-card-default uk-grid-collapse <?= Yii::$app->settings->get('blog', 'postCssClass') ?>" uk-grid>
    <?php if ($model->image): ?>
    <div class="uk-card-media-left uk-cover-container uk-width-1-2@m">
        <?= Html::img($model->getImageUrl('medium'), ['alt' => $title, 'uk-cover' => true]) ?>
        <canvas width="600" height="400"></canvas>
    </div>
    <?php endif; ?>
    <div class="uk-width-expand@m">
        <div class="uk-card-body">
            <h3 class="uk-card-title"><?= Html::a($title, $link) ?></h3>
            <?php if ($meta): ?>
            <p class="uk-article-meta"><?= implode(' | ', $meta) ?></p>
            <?php endif; ?>
            <p><?= $model->small_text ?></p>
        </div>
    </div>
</div>
