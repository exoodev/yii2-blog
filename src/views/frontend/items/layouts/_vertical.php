<?php
use yii\helpers\Html;

$title = Html::encode($model->title);
?>

<div class="uk-card uk-card-default <?= Yii::$app->settings->get('blog', 'postCssClass') ?>">
    <?php if ($model->image): ?>
    <div class="uk-card-media-top">
        <?= Html::img($model->getImageUrl('medium'), ['alt' => $title]) ?>
    </div>
    <?php endif; ?>
    <div class="uk-card-body">
        <h3 class="uk-card-title"><?= Html::a($title, $link) ?></h3>
        <?php if ($meta): ?>
        <p class="uk-article-meta"><?= implode(' | ', $meta) ?></p>
        <?php endif; ?>
        <p><?= $model->small_text ?></p>
    </div>
</div>
