<?php

use yii\helpers\Html;

$this->title = $category->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Blog'), 'url' => ['post/index']];
$this->params['breadcrumbs'][] = $this->title;

?>


<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('/items/list', [
    'dataProvider' => $dataProvider,
]) ?>
