<?php

use yii\helpers\Html;
use exoo\grid\GridView;

$this->title = Yii::t('blog', 'Categories');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('blog', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ]),
        Html::a(Yii::t('blog', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('blog', 'Are you sure you want to delete the selected items?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => 'exoo\grid\CheckboxColumn'],
        [
            'attribute' => 'id',
            'format' => 'html',
            'headerOptions' => ['class' => 'uk-table-shrink'],
            'value' => function($model) {
                return Html::a($model->id, ['update', 'id' => $model->id]);
            },
        ],
        'title',
        'slug',
        ['class' => 'exoo\position\PositionColumn'],
        ['class' => 'exoo\status\StatusColumn'],
    ],
]); ?>
