<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>

<div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
    </div>
    <div>
        <?= $form
            ->field($model, 'status', ['options' => ['class' => 'uk-float-left uk-margin-remove']])
            ->dropDownList($model->statusList)
            ->label(false) ?>
        <?= Html::submitButton(Yii::t('blog', 'Save'), [
            'class' => 'uk-button uk-button-success uk-margin-small-left'
        ]) ?>
        <?= Html::a(Yii::t('blog', 'Close'), ['index'], [
            'class' => 'uk-button uk-button-default'
        ]) ?>
        <?php if (!$model->isNewRecord): ?>
            <?= Html::a(Yii::t('blog', 'Delete'), ['delete', 'id' => $model->id], [
                'data-method' => 'post',
                'data-confirm' => Yii::t('blog', 'Are you sure want to delete this category?'),
                'class' => 'uk-button uk-button-danger',
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<div class="uk-card uk-card-default uk-card-body">
    <div class="uk-child-width-1-2@m" uk-grid>
        <div>
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
        </div>
        <div>
            
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
