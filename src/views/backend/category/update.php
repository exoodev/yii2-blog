<?php

$this->title = Yii::t('blog', 'Update Category') . ': '. $model->title;
?>

<?= $this->render('_form', ['model' => $model]) ?>
