<?php

$this->title = Yii::t('blog', 'Create Category');
?>

<?= $this->render('_form', ['model' => $model]) ?>
