<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\ActiveForm;

$this->title = Yii::t('blog', 'Settings');
$positions = ArrayHelper::getValue(Yii::$app->params, 'positions', []);
?>

<div class="uk-card uk-card-default uk-card-body">
    <?php $form = ActiveForm::begin(); ?>
        <div class="tm-sticky-subnav uk-flex uk-flex-between" uk-margin>
            <div>
                <div class="uk-card-title"><?= Html::encode($this->title) ?></div>
            </div>
            <div>
                <?= Html::submitButton(Yii::t('blog', 'Save'), [
                    'class' => 'uk-button uk-button-success'
                ]) ?>
            </div>
        </div>
        <ul uk-tab>
            <li><a href="#posts"><?= Yii::t('blog', 'Posts') ?></a></li>
            <li><a href="#categories"><?= Yii::t('blog', 'Categories') ?></a></li>
        </ul>
        <ul class="uk-switcher uk-margin">
            <li>
                <div class="uk-width-1-3@m">
                    <?= $form->field($model, 'titleBlog')->textInput() ?>
                    <?= $form->field($model, 'postsOnPage')->textInput() ?>
                    <?= $form->field($model, 'showGuestPosts')->checkbox() ?>
                    <?= $form->field($model, 'showAuthor')->checkbox() ?>
                    <?= $form->field($model, 'showPublishDate')->checkbox() ?>
                    <?= $form->field($model, 'postsLayout')->dropDownList($model->postsLayouts) ?>
                    <?= $form->field($model, 'postsColumns')->dropDownList(array_combine(range(1, 5), range(1, 5))) ?>
                    <?= $form->field($model, 'postCssClass')->textInput() ?>
                </div>
            </li>
            <li>
                <div class="uk-width-1-3@m">
                    <?= $form->field($model, 'categoryCssClass')->textInput() ?>
                    <?= $form->field($model, 'categoryPosition')->dropDownList(array_combine($positions, $positions)) ?>
                </div>
            </li>
        </ul>
    <?php ActiveForm::end(); ?>
</div>
