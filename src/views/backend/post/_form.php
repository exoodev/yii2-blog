<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\uikit\Switcher;
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= $form
                ->field($post, 'status_id', ['options' => ['class' => 'uk-float-left uk-margin-remove']])
                ->dropDownList($post->statuses)
                ->label(false) ?>
            <?= Html::submitButton(Yii::t('blog', 'Save'), [
                'class' => 'uk-button uk-button-success uk-margin-small-left'
            ]) ?>
            <?= Html::a(Yii::t('blog', 'Close'), ['index'], [
                'class' => 'uk-button uk-button-default'
            ]) ?>
            <?php if (!$post->isNewRecord): ?>
                <?= Html::a(Yii::t('blog', 'Delete'), ['delete', 'id' => $post->id], [
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('blog', 'Are you sure want to delete this post?'),
                    'class' => 'uk-button uk-button-danger',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body">
        <?= Switcher::widget([
            'type' => 'tab',
            'items' => [
                [
                    'label' => Yii::t('blog', 'Post'),
                    'content' => $this->render('_formContent', [
                        'form' => $form,
                        'post' => $post,
                        'categories' => $categories,
                        'users' => $users,
                    ]),
                ],
                [
                    'label' => Yii::t('blog', 'Meta'),
                    'content' => $this->render('_formMeta', [
                        'form' => $form,
                        'post' => $post,
                    ]),
                ],
            ],
        ]) ?>
    </div>
<?php ActiveForm::end(); ?>
