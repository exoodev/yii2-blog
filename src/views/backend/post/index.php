<?php

use yii\helpers\Html;
use exoo\grid\GridView;

$this->title = Yii::t('blog', 'Posts');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('blog', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ]),
        Html::a(Yii::t('blog', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('blog', 'Are you sure you want to delete the selected items?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => 'exoo\grid\CheckboxColumn'],
        [
            'attribute' => 'id',
            'format' => 'html',
            'headerOptions' => ['class' => 'uk-table-shrink'],
            'value' => function($model) {
                return Html::a($model->id, ['update', 'id' => $model->id]);
            },
        ],
        'title',
        [
            'attribute' => 'author.username',
            'label' => $searchModel->getAttributeLabel('author'),
            'format' => 'html',
            'value' => function($model) {
                return Html::encode($model->author->username);
            },
        ],
        [
            'attribute' => 'category.title',
            'label' => Yii::t('blog', 'Category'),
            'format' => 'html',
            'value' => function($model) {
                if ($model->category !== null) {
                    $title = Html::encode($model->category->title);
                    return Html::a($title, ['/blog/category/update', 'id' => $model->category_id]);
                }
            },
        ],
        [
            'attribute' => 'updated_at',
            'filter' => false,
            'value' => function($model) {
                return Yii::$app->formatter->asDatetime($model->updated_at);
            },
        ],
        [
            'attribute' => 'status_id',
            'filter' => $searchModel->statuses,
            'format' => 'raw',
            'filterInputOptions' => ['prompt' => 'Все'],
            'value' => function ($model) {
                return Html::tag('div', $model->status, [
                    'class' => 'uk-label uk-label-' . $model->statusCssClass,
                    'uk-tooltip' => $model->status
                ]);
            },
        ],
        [
            'class' => 'exoo\grid\ActionColumn',
            'template' => '{update} {delete}'
        ],
    ],
]); ?>
