<?php

$this->title = Yii::t('blog', 'Update Post');
?>
<?= $this->render('_form', [
    'post' => $post,
    'categories' => $categories,
    'users' => $users,
]) ?>
