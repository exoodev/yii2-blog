<?= $form->field($post, 'meta_title')->textInput(['maxlength' => true]) ?>
<?= $form->field($post, 'meta_description')->textarea(['rows' => 5, 'maxlength' => true]) ?>
