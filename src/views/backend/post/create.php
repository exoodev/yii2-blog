<?php

$this->title = Yii::t('blog', 'Create Post');
?>
<?= $this->render('_form', [
    'post' => $post,
    'categories' => $categories,
    'users' => $users,
]) ?>
