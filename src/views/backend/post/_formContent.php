<?php

use yii\helpers\ArrayHelper;
use exoo\joditeditor\JoditEditor;
use exoo\datepicker\DatePicker;
use exoo\timepicker\TimePicker;
use exoo\storage\widgets\FileInput;
?>
<div uk-grid>
    <div class="uk-width-expand@m">
        
        <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
            <div>
                <?= $form->field($post, 'title')->textInput([
                    'maxlength' => true,
                    'placeholder' => $post->getAttributeLabel('title'),
                ]) ?>
            </div>
            <div>
                <?= $form->field($post, 'slug')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <?= $form->field($post, 'small_text')->widget(JoditEditor::className()) ?>
        <?= $form->field($post, 'full_text')->widget(JoditEditor::className(), [
            'clientOptions' => [
                'height' => '600px',
            ]
        ]) ?>
    </div>
    <div class="uk-width-1-4@m">
        <?= $form->field($post, 'category_id')->dropdownList(ArrayHelper::map($categories, 'id', 'title'), ['prompt' => '']) ?>
        <?= $form->field($post, 'created_by')->dropdownList(ArrayHelper::map($users, 'id', 'username'), ['prompt' => '']) ?>
        <?= $form->field($post, 'image')->widget(FileInput::className(), [
            'url' => ['image'],
            'clientOptions' => [
                'mime' => 'image/*',
                'cropperOptions' => [
                    'aspectRatio' => 16 / 9
                ]
            ]
        ]) ?>
        <?= $form
            ->field($post, 'publishOnDate')
            ->widget(DatePicker::className())
            ->icon('calendar')
        ?>
        <?= $form
            ->field($post, 'publishOnTime')
            ->widget(TimePicker::className(), ['timeFormat' => 'HH:mm'])
            ->icon('clock')
        ?>
    </div>
</div>


<?php
$js = <<<JS
// var selectMedia = $('#post-image_url-container').selectMedia();
//
// $(document).on('click', '.ex-media-insert', function(e) {
//     console.log($('#post-image_url-container').selectMedia('getValue'))
// })
JS;
$this->registerJs($js);
