<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models;

/**
 * This is the ActiveQuery class for [[Post]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class PostQuery extends \yii\db\ActiveQuery
{
    /**
     * Select active.
     *
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status_id' => Post::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * Select inactive.
     *
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status_id' => Post::STATUS_INACTIVE]);

        return $this;
    }

    /**
     * Publish on.
     *
     * @return $this
     */
    public function published()
    {
        $this->andWhere(['<', 'publish_on', time()]);

        return $this;
    }

}
