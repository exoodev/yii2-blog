<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models;

use Yii;
use yii\base\Model;

/**
 * Setting class for configuration management for the extension Blog.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Settings extends Model
{
    const POSTS_LAYOUT_HORIZONTAL = 'horizontal';
    const POSTS_LAYOUT_VERTICAL = 'vertical';

    /**
     * @var integer count of posts on page.
     */
    public $postsOnPage = 10;
    /**
     * @var boolean whether to show posts to guest.
     */
    public $showGuestPosts = true;
    /**
     * @var boolean show author.
     */
    public $showAuthor = true;
    /**
     * @var boolean show publish date.
     */
    public $showPublishDate = true;
    /**
     * @var string the сategory position
     */
    public $categoryPosition = 'main-bottom';
    /**
     * @var string the post css class
     */
    public $postCssClass;
    /**
     * @var string the сategory css class
     */
    public $categoryCssClass;
    /**
     * @var string the posts Layout
     */
    public $postsLayout = 'vertical';
    /**
     * @var string the number of columns in the list of posts
     */
    public $postsColumns = 3;

    /** @var String $titleBlog the primary title blog */
    public $titleBlog = 'Blog';

    /**
     * Returns the validation rules for attributes.
     * @return array Validation rules.
     */
    public function rules()
    {
        return [
            [['postsOnPage', 'postsColumns'], 'integer'],
            [['showGuestPosts', 'showAuthor', 'showPublishDate'], 'boolean'],
            [['titleBlog', 'categoryPosition', 'categoryCssClass', 'postsLayout', 'postCssClass'], 'string', 'max' => 255],
        ];
    }

    /**
     * Returns the attribute labels.
     * @return array Attribute labels (name => label).
     */
    public function attributeLabels()
    {
        return [
            'titleBlog' => Yii::t('blog', 'Title blog'),
            'postsOnPage' => Yii::t('blog', 'Post count on the page'),
            'showGuestPosts' => Yii::t('blog', 'Show posts to guests'),
            'showAuthor' => Yii::t('blog', 'Show author'),
            'showPublishDate' => Yii::t('blog', 'Show publish date'),
            'categoryPosition' => Yii::t('blog', 'Category position'),
            'categoryCssClass' => Yii::t('blog', 'Css class category'),
            'postsLayout' => Yii::t('blog', 'Layout for posts'),
            'postsColumns' => Yii::t('blog', 'Number of columns in the list of posts'),
            'postCssClass' => Yii::t('blog', 'Css class post'),
        ];
    }

    /**
     * Posts Layouts
     * @return array
     */
    public static function getPostsLayouts()
    {
        return [
            self::POSTS_LAYOUT_HORIZONTAL => Yii::t('blog', 'Horizontal'),
            self::POSTS_LAYOUT_VERTICAL => Yii::t('blog', 'Vertical'),
        ];
    }

    /**
     * Returns current posts layout.
     * @return string status of the model.
     */
    public function getPostsLayout()
    {
        $layouts = self::getPostsLayouts();
        return $layouts[$this->postsLayout];
    }
}
