<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models;

/**
 * This is the ActiveQuery class for [[Category]].
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class CategoryQuery extends \yii\db\ActiveQuery
{
    /**
     * Select active.
     *
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => Category::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * Select inactive.
     *
     * @return $this
     */
    public function inactive()
    {
        $this->andWhere(['status' => Category::STATUS_INACTIVE]);

        return $this;
    }
}
