<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models\backend;

use yii\data\ActiveDataProvider;
use exoo\blog\models\Post;

/**
 * PostSearch is the class used to search and filter the posts.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'author.username',
            'category.title',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'author.username', 'category.title'], 'safe'],
        ];
    }

    /**
     * Returns instance of the class ActiveDataProvider with introduced search query.
     * @param array $params Search parameters.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find()
            ->from(['p' => Post::tableName()])
            ->joinWith(['author a', 'category c']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'p.id' => $this->id,
            'p.status_id' => $this->status_id,
            'p.created_at' => $this->created_at,
            'p.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'p.title', $this->title])
            ->andFilterWhere(['like', 'a.username', $this->getAttribute('author.username')])
            ->andFilterWhere(['like', 'c.title', $this->getAttribute('category.title')]);
        return $dataProvider;
    }
}
