<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii2tech\ar\position\PositionBehavior;
use exoo\status\StatusTrait;

/**
 * Category is the class for extension Blog.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Category extends \yii\db\ActiveRecord
{
    use StatusTrait;
    /**
     * Status of the category, if it is not active.
     */
    const STATUS_INACTIVE = 0;
    /**
     * Status of the category, if it is active.
     */
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['status', 'position'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('blog', 'ID'),
            'title' => Yii::t('blog', 'Title'),
            'slug' => Yii::t('blog', 'Slug'),
            'status' => Yii::t('blog', 'Status'),
            'position' => Yii::t('blog', 'Position'),
        ];
    }

    /**
     * @inheritdoc
     * @return BlogCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'sluggableBehavior' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
            ],
        ];
    }

    public static function getStatuses(): array
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('blog', 'Inactive'),
                'class' => 'off ex-text-grey-300',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('blog', 'Active'),
                'class' => 'on uk-text-success',
            ],
        ];
    }

    /**
     * Returns a value indicating whether the current model is active.
     * @return boolean if model is active value will be `true`, else `false`.
     */
    public function getIsActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * Declares a `has-many` relation with class [[\exoo\blog\models\Post]].
     * @return array The related objects of the class [[\exoo\blog\models\Post]].
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['category_id' => 'id']);
    }
}
