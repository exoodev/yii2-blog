<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * Post is the class for extension Blog.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Post extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    /**
     * Status of the post, if it is not active.
     */
    const STATUS_INACTIVE = 0;
    /**
     * Status of the post, if it is active.
     */
    const STATUS_ACTIVE = 1;
    /**
     * Status of the post, if it is on moderation.
     */
    const STATUS_MODERATION = 2;
    /**
     * Status of the post, if it is draft.
     */
    const STATUS_DRAFT = 3;

    /**
     * @var string the publish date.
     */
    private $_publishOnDate;

    /**
     * @var string the publish time.
     */
    private $_publishOnTime;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_post}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'small_text', 'full_text'], 'required'],
            [['publishOnDate', 'publishOnTime'], 'required', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            ['publishOnDate', 'date', 'format' => 'yyyy-MM-dd'],
            ['publishOnTime', 'time', 'format' => 'HH:mm'],
            [[
                'category_id',
                'status_id',
                'created_by',
                'updated_by',
                'created_at',
                'updated_at',
                'publish_on',
            ], 'integer'],
            [['small_text', 'full_text'], 'string'],
            [['title', 'slug', 'image'], 'string', 'max' => 255],
            ['status_id', 'default', 'value' => self::STATUS_INACTIVE],
            ['status_id', 'in', 'range' => array_keys(self::getStatuses())],
            ['meta_title', 'string', 'max' => 70],
            ['meta_description', 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('blog', 'ID'),
            'title' => Yii::t('blog', 'Title'),
            'slug' => Yii::t('blog', 'Slug'),
            'small_text' => Yii::t('blog', 'Preview'),
            'full_text' => Yii::t('blog', 'Content'),
            'publish_on' => Yii::t('blog', 'Publish on'),
            'publishOnDate' => Yii::t('blog', 'Date of publication'),
            'publishOnTime' => Yii::t('blog', 'Time of publication'),
            'category_id' => Yii::t('blog', 'Category'),
            'status_id' => Yii::t('blog', 'Status'),
            'created_by' => Yii::t('blog', 'Created by'),
            'updated_by' => Yii::t('blog', 'Updated by'),
            'created_at' => Yii::t('blog', 'Created at'),
            'updated_at' => Yii::t('blog', 'Updated at'),
            'image' => Yii::t('blog', 'Image'),
            'author' => Yii::t('blog', 'Author'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true,
            ],
        ];
    }

    /**
     * Returns the model publication date.
     * @return string Publication date.
     */
    public function getPublishOnDate()
    {
        if ($this->_publishOnDate === null) {
            $this->_publishOnDate = $this->isNewRecord ? time() : $this->publish_on;
        }

        return $this->_publishOnDate;
    }

    /**
     * Sets publish date.
     *
     * @param mixed $value
     */
    public function setPublishOnDate($value)
    {
        $this->_publishOnDate = $value;
    }

    /**
     * Returns the model publication time.
     * @return string Publication time.
     */
    public function getPublishOnTime()
    {
        if ($this->_publishOnTime === null) {
            $this->_publishOnTime = $this->isNewRecord ? time() : $this->publish_on;
        }

        return $this->_publishOnTime;
    }

    /**
     * Set publication time.
     *
     * @param mixed $value
     */
    public function setPublishOnTime($value)
    {
        $this->_publishOnTime = $value;
    }

    /**
     * Statuses used in class.
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_INACTIVE => Yii::t('blog', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('blog', 'Active'),
            self::STATUS_MODERATION => Yii::t('blog', 'Moderation'),
            self::STATUS_DRAFT => Yii::t('blog', 'Draft'),
        ];
    }

    /**
     * Returns current status of the model.
     * @return string status of the model.
     */
    public function getStatus()
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status_id];
    }

    /**
     * Css classes related with statuses.
     * @return array
     */
    public static function getStatusesCssClasses()
    {
        return [
            self::STATUS_INACTIVE => 'danger',
            self::STATUS_ACTIVE => 'success',
            self::STATUS_MODERATION => 'warning',
            self::STATUS_DRAFT => 'default',
        ];
    }

    /**
     * Returns the model css class.
     * @return string css class depends on the status of the model.
     */
    public function getStatusCssClass()
    {
        $classes = self::getStatusesCssClasses();
        return $classes[$this->status_id];
    }

    /**
     * Returns a value indicating whether the current model is active.
     * @return boolean if model is active value will be `true`, else `false`.
     */
    public function getIsActive()
    {
        return $this->status_id === self::STATUS_ACTIVE;
    }

    /**
     * Declares a `has-one` relation with class [[User]].
     * @return User The related object of the class [[User]].
     */
    public function getAuthor()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * Declares a `has-one` relation with class [[\exoo\blog\models\Category]].
     * @return \exoo\blog\models\Category The related object of the class [[\exoo\blog\models\Category]].
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Get image url
     * @return string the url
     */
    public function getImageUrl($size)
    {
        return Yii::$app->fileStorage->getFileUrl('blog', $this->image, $size);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->slug === null) {
                $this->slug = $this->id;
            }
            if ($this->_publishOnDate && $this->_publishOnTime) {
                $this->publish_on = Yii::$app->formatter->asTimestamp($this->_publishOnDate . ' ' . $this->_publishOnTime);
            }

            return true;
        } else {
            return false;
        }
    }
}
