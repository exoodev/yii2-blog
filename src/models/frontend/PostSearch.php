<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\models\frontend;

use Yii;
use yii\data\ActiveDataProvider;
use exoo\blog\models\Post;
use exoo\blog\models\Category;

/**
 * PostSearch is the class used to search and filter the posts.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['title'], 'safe'],
        ];
    }

    /**
     * @param integer $categoryId category id.
     */
    public function search($params, $category_id = false)
    {
        $query = Post::find()->with('author')->published()->active();

        if ($category_id) {
            $query->andWhere(['category_id' => $category_id])->with('category');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'publish_on' => SORT_DESC,
                ]
            ],
            'pagination' => [
                'pageSize' => Yii::$app->settings->get('blog', 'postsOnPage'),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
