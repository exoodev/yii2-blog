<?php

namespace exoo\blog;

use Yii;

/**
 * Blog module for application.
 */
class Module extends \yii\base\Module
{
    /**
     * @var string the extension name.
     */
    public $name;
    /**
     * @var string the bucket name
     */
    public $bucketName = 'blog';
    /**
     * @var array the config storage bucket
     */
    public $bucketData = [
        'baseSubPath' => '_blog',
        'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
    ];
    /**
     * @var boolean
     */
    public $isBackend = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->name = Yii::t('blog', 'Blog');
        Yii::$app->fileStorage->addBucket($this->bucketName, $this->bucketData);

        if ($this->isBackend === true) {
            $this->setViewPath('@exoo/blog/views/backend');
            if ($this->controllerNamespace === null) {
                $this->controllerNamespace = 'exoo\blog\controllers\backend';
            }
        } else {
            $this->setViewPath('@exoo/blog/views/frontend');
            if ($this->controllerNamespace === null) {
                $this->controllerNamespace = 'exoo\blog\controllers\frontend';
            }
        }

        parent::init();
    }
}
