<?php

/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\controllers\frontend;

use Yii;
use exoo\blog\models\Post;
use exoo\blog\models\Category;
use exoo\blog\models\frontend\PostSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\AccessControl;

/**
 * Post controller of module Blog for frontend application.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => Yii::$app->settings->get('blog', 'showGuestPosts') ? ['?', '@'] : ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays all posts.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays the post.
     * @return mixed
     * @throws HttpException If post is not found or if the category which related with loaded post is not active.
     */
    public function actionView($slug, $category = false)
    {
        $model = Post::find()
            ->with([
                'author',
                'category' => function ($q) use ($category) {
                    $q->where(['slug' => $category]);
                }
            ])
            ->where([
                'slug' => $slug,
            ])
            ->published()
            ->active()
            ->one();

        if ($model === null || ($model->category != null && $model->category->status == Category::STATUS_INACTIVE)) {
            throw new HttpException(404, 'The requested Item could not be found.');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
