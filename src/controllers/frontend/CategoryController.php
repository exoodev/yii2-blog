<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\controllers\frontend;

use Yii;
use exoo\blog\models\Category;
use exoo\blog\models\frontend\PostSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\AccessControl;

/**
 * Category controller of module Blog for frontend application.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view'],
                'rules' => [
                    [
                        'allow' => false,
                        'verbs' => ['POST']
                    ],
                    [
                        'allow' => true,
                        'roles' => Yii::$app->settings->get('blog', 'showGuestPosts') ? ['?', '@'] : ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays all posts of the category.
     * @return mixed
     * @throws HttpException If category is not found.
     */
    public function actionView($slug)
    {
        $category = Category::find()
            ->where(['slug' => $slug])
            ->active()
            ->one();

        if ($category === null) {
            throw new HttpException(404, 'The requested Item could not be found.');
        }

        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $category->id);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }
}
