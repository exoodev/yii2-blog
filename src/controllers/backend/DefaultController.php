<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\controllers\backend;

use Yii;
use yii\web\Controller;
use exoo\blog\models\Settings;

/**
 * Default Controller
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'settings' => [
                'class' => 'exoo\settings\actions\SettingsAction',
                'modelClass' => Settings::className(),
            ]
        ];
    }
}
