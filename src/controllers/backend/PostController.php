<?php

namespace exoo\blog\controllers\backend;

use Yii;
use exoo\blog\models\Post;
use exoo\blog\models\Category;
use exoo\blog\models\backend\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;

/**
 * Post Controller
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'batchDelete' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'image' => Yii::$app->fileStorage->action('blog.image', [
                'modelClass' => 'exoo\blog\models\Post',
                'bucketName' => 'blog',
                'fileAttribute' => 'image',
                'transformImage' => [
                    'thumbnail' => [
                        'large' => ['width' => 1200, 'height' => 675],
                        'medium' => ['width' => 800, 'height' => 450],
                    ],
                ],
            ]),
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => Post::getStatuses(),
        ]);
    }

    /**
     * Creates a new [[Post]] model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $post = new Post(['scenario' => Post::SCENARIO_CREATE]);

        if ($post->load(Yii::$app->request->post()) && $post->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Saved'));
            return $this->redirect(['update', 'id' => $post->id]);
        }
        $categories = Category::find()->all();
        $users = Yii::$app->user->identityClass::find()->active()->all();

        return $this->render('create', [
            'post' => $post,
            'categories' => $categories,
            'users' => $users,
        ]);
    }

    /**
     * Updates an existing [[Post]] model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Post id.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $post = $this->findModel($id);
        $post->scenario = Post::SCENARIO_UPDATE;
        $categories = Category::find()->all();
        $users = Yii::$app->user->identityClass::find()->active()->all();

        if ($post->load(Yii::$app->request->post()) && $post->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Saved'));
        }

        return $this->render('update', [
            'post' => $post,
            'categories' => $categories,
            'users' => $users,
        ]);
    }

    /**
     * Changing the status of the [[Post]] model on the opposite.
     * @param integer $id Post id.
     * @return mixed
     */
    public function actionStatus($id)
    {
        $model = $this->findModel($id);

        if (!$model->isActive) {
            $model->status_id = Post::STATUS_ACTIVE;
        } else {
            $model->status_id = Post::STATUS_INACTIVE;
        }

        $model->save();

        return $this->goBack();
    }

    /**
     * Deletes an existing [[Post]] model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Deleted'));
        }

        return $this->goBack();
    }

    /**
     * Deletes multiple an existing [[Post]] models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            if (Post::deleteAll(['id' => $ids])) {
                Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Deleted'));
            }
            return $this->goBack();
        } else {
            throw new BadRequestHttpException(400);
        }
    }

    /**
     * Finds the [[Post]] model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id Post id.
     * @return Post The loaded model.
     * @throws NotFoundHttpException If the model is not found.
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
