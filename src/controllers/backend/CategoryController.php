<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use exoo\blog\models\Category;
use exoo\blog\models\backend\CategorySearch;
use exoo\position\PositionAction;
use exoo\status\StatusAction;

/**
 * Manages categories of the extension Blog.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'delete' => ['post'],
                    'batchDelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'status' => [
                'class' => StatusAction::class,
                'modelClass' => 'exoo\blog\models\Category',
            ],
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => 'exoo\blog\models\Category',
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new [[Category]] model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Saved'));

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing [[Category]] model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id Category id.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Saved'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing [[Category]] model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id Category id.
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Deleted'));
        }

        return $this->goBack();
    }

    /**
     * Deletes multiple an existing [[Category]] models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            if (Category::deleteAll(['id' => $ids])) {
                Yii::$app->session->setFlash('notify.success', Yii::t('blog', 'Deleted'));
            }
            return $this->goBack();
        } else {
            throw new BadRequestHttpException(400);
        }
    }

    /**
     * Finds the [[Category]] model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id Category id.
     * @return Category the loaded model.
     * @throws NotFoundHttpException if the model cannot be found.
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
