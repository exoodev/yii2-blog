<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\blog\widgets;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use exoo\uikit\Nav;
use exoo\blog\models\Category;

/**
 * Category sidebar menu.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class CategoryNav extends Widget
{
    /**
     * @var string the property
     */
    public $items = [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (!$this->items) {
            $categories = Category::find()->active()->all();
            $result = [];
            foreach ($categories as $category) {
                $result[] = [
                    'url' => ['category/view', 'slug' => $category->slug],
                    'label' => $category->title,
                ];
            }

            $this->items = $result;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        if (!$this->items) {
            return;
        }

        $nav = Nav::widget([
            'items' => $this->items,
            'type' => 'navside',
            'accordion' => false,
        ]);
        $cardOptions = [
            'class' => 'uk-card uk-card-default uk-card-body'
        ];

        if ($class = Yii::$app->settings->get('blog', 'categoryCssClass')) {
            Html::addCssClass($cardOptions, $class);
        }

        $title = Html::tag('h3', Yii::t('blog', 'Categories'), ['class' => 'uk-card-title']) . "\n";

        $this->view->beginBlock(Yii::$app->settings->get('blog', 'categoryPosition'));
        echo Html::tag('div', $title . $nav, $cardOptions);
        $this->view->endBlock();
    }
}
