<?php

namespace exoo\blog;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Blog extension bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		$app->i18n->translations['blog'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@exoo/blog/messages',
		];

		if (!$app->getModule('blog')->isBackend) {
			Yii::$app->getUrlManager()->addRules([
				'<_m:blog>' => '<_m>/post/index',
				'<_m:blog>/category/<slug:[a-zA-Z0-9_-]{1,100}+>' => '<_m>/category/view',
				'<_m:blog>/<category:[a-zA-Z0-9_-]{1,100}+>/<slug:[a-zA-Z0-9_-]{1,100}+>' => '<_m>/post/view',
				'<_m:blog>/post/<slug:[a-zA-Z0-9_-]{1,100}+>' => '<_m>/post/view',
			], false);
		}
	}
}
