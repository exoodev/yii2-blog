<?php

$subnav = [
    [
        'label' => Yii::t('blog', 'Posts'),
        'url' => ['/blog/post/index'],
        'routeActive' => 'blog/post',
    ],
    [
        'label' => Yii::t('blog', 'Categories'),
        'url' => ['/blog/category/index'],
        'routeActive' => 'blog/category',
    ],
    [
        'label' => Yii::t('blog', 'Settings'),
        'url' => ['/blog/default/settings'],
        'routeActive' => 'blog/default',
    ]
];

return [
    'navside' => [
        [
            'icon' => '<i class="fas fa-book fa-lg fa-fw uk-margin-small-right"></i>',
            'label' => Yii::t('blog', 'Blog'),
            'items' => $subnav,
        ]
    ],
    'subnav' => $subnav,
];
