<?php

namespace exoo\blog\components;

use yii\rbac\Rule;

/**
 * Checks if authorID matches user passed via params
 */
class AuthorRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'isAuthor';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return isset($params['model']) ? $params['model']->created_by == $user : false;
    }
}
