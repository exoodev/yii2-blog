# Blog.
========================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist exoodev/yii2-blog
```

or add

```json
"exoodev/yii2-blog": "*"
```

to the require section of your composer.json.



Usage
-----

Example backend configuration:

```php
return [
    'modules' => [
        'blog' => [
            'class' => 'exoo\blog\Module',
            'isBackend' => true,
        ],
    ],
];
```

Example frontend configuration:

```php
return [
    'modules' => [
        'blog' => [
            'class' => 'exoo\blog\Module',
        ]
    ],
];
```

## Migration

```
yii migrate --migrationPath=@exoo/blog/migrations
```
